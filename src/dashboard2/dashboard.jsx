import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import axios from 'axios'

import ContentHeader from '../common/template/contentHeader'
import Content from '../common/template/content'
import ValueBox from '../common/widget/valueBox'

const BASE_URL = 'http://localhost:3003/api'

class Dashboard extends Component {

    constructor(props) {
        super(props)
        this.state = {credit: 0, debt: 1}
    }
    componentWillMount() {
        const request = axios.get(`${BASE_URL}/billingCycles/summary`)
            .then(rest => this.setState(resp.data))
    }

    render() {
        const {credit, debt} = this.state
        return (
            <div>
                <ContentHeader title="Dashboard" small="Versão 0.0.1" />        
                <Content>
                    <ValueBox cols="12 4" color="green" icon="banck" 
                        value={`R$ ${credit}`} text="Total de Créditos" />
                    <ValueBox cols="12 4" color="red" icon="credit-card" 
                        value={`R$ ${debt}`} text="Total de Débido" />
                    <ValueBox cols="12 4" color="blue" icon="money" 
                        value={`R$ ${credit - debt}`} text="Valor Consolidado" />
                </Content>
            </div>
        )
    }
}

//const mapStateToProps = state => ({summary: state.dashboard.summary})
//const mapDispatchToProps = dispatch => bindActionCreators({getSummary}, dispatch)
export default Dashboard