export function selectTab(tabId) {
    console.log('Selected ' + tabId)
    return {
        type: 'TAB_SELECTED',
        payload: tabId
    }
}

export function showTabs(...tabIds) {
    const tabsToShow = {}
    tabIds.forEach(e => tabsToShow[e] = true)
    console.log('Show ',tabsToShow)
    return {
        type: 'TAB_SHOWED',
        payload: tabsToShow
    }
}