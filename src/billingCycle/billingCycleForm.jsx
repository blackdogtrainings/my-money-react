import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { reduxForm, Field, formValueSelector } from 'redux-form'

import labelAndInput from '../common/form/labelAndInput'
import { init, test } from './billingCycleActions' 

class BillingCycleForm extends Component {
    render() {
        const { handleSubmit } = this.props
        return (
            <form role='form' onSubmit={handleSubmit}>
                <div className='box-body'>
                    <Field name='name' component={labelAndInput} 
                        label='Nome' cols='12 4' placeholder='Informe o nome'/>
                    <Field name='month' component={labelAndInput} 
                        label='Mês' cols='12 4' placeholder='Informe o mês'/>
                    <Field name='year' component={labelAndInput} 
                        label='Ano' cols='12 4' placeholder='Informe o ano'/>
                </div>
                <div className='box-footer'>
                    <button type='submit' className='btn btn-primary'>submit</button>
                    <button type='button' onClick={this.props.init} className='btn btn-default'>Cancelar</button>
                </div>

            </form>
        )
    }
}

BillingCycleForm = reduxForm({form: 'billingCycleForm', destroyOnUnmount: false})(BillingCycleForm)
const mapDispatchToProps = dispatch => bindActionCreators({ init, test}, dispatch)
export default connect(null, mapDispatchToProps)(BillingCycleForm)